<?php

namespace BackendDev\Api;

Interface UserRepositoryInterface
{
    /**
     * @param \BackendDev\Api\Data\UsersInterface $data
     * @return $this
     */
    public function create($data);
}