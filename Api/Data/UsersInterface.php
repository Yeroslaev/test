<?php
namespace BackendDev\Api\Data;

Interface UsersInterface
{
    const ID = 'entity_id';
    const LOGIN = 'login';
    const PASSWORD = 'password';
    const EMAIL = 'email';
    const USERNAME = 'username';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return string
     */
    public function getLogin();

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin($login);

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username);
}