<?php
namespace BackendDev\Api\Data;

Interface TicketsInterface
{
    const ID = 'entity_id';
    const USER = 'user_id';
    const STATUS = 'status';
    const TITLE = 'title';
    const MESSAGE = 'message';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';


    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $date
     * @return $this
     */
    public function setCreatedAt($date);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string $date
     * @return $this
     */
    public function setUpdatedAt($date);
}