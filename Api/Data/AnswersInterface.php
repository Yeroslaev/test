<?php
namespace BackendDev\Api\Data;

Interface AnswersInterface
{
    const ID = 'entity_id';
    const TICKET = 'ticket_id';
    const USER = 'user_id';
    const ANSWER = 'answer';
    const CREATED = 'created_at';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getTicketId();

    /**
     * @param int $ticketId
     * @return $this
     */
    public function setTicketId($ticketId);

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * @return string
     */
    public function getAnswer();

    /**
     * @param string $answer
     * @return $this
     */
    public function setAnswer($answer);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);
}