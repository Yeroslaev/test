<?php
namespace BackendDev\Api\Data;

Interface DataObjectInterface
{
    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setData($key, $value);

    /**
     * @param string $key
     * @return mixed
     */
    public function getData($key);

    /**
     * @param string $key
     * @return $this
     */
    public function unsetData($key);
}