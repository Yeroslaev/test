<?php
namespace BackendDev\Config;

class Connector extends \PDO
{
    /**
     * @var string
     */
    protected $_host;

    /**
     * @var string
     */
    protected $_user;

    /**
     * @var string
     */
    protected $_pass;

    /**
     * @var string
     */
    protected $_port;

    /**
     * @var string
     */
    protected $_name;

    /**
     * @var array
     */
    protected $_config;

    /**
     * oDb constructor.
     * @param string $config
     */
	public function __construct($config = 'config.ini') 
	{
		$this->_config = parse_ini_file($config);
		$driver = $this->_config['driver'];
		$this->_host = $this->_config['dbhost'];
		$this->_user = $this->_config['dbuser'];
		$this->_pass = $this->_config['dbpass'];
		$this->_port = $this->_config['dbport'];
		$this->_name = $this->_config['dbbase'];
		parent::__construct("$driver:host=$this->_host;port=$this->_port;dbname=$this->_name",$this->_user, $this->_pass);
	}

    /**
     * @param string $config
     * @return string|bool
     */
	public function getConfig($config)
    {
        $db = $this->prepare("SELECT config_value FROM {$this->_config['table_configs']} WHERE config_name = :config && approve = 1");
        return ($db->execute(['config' => $config])) ? $db->fetch()[0] : false;
    }

    /**
     * @param string $username
     * @return bool
     */
    public function checkUsername($username)
    {
        foreach([$this->_config['table_users'], $this->_config['table_operators']] as $table) {
            $db = $this->prepare("SELECT count(*) as cnt from {$table} where username = :username");
            $db->execute(['username' => $username]);
            if($db->fetch()[0]['cnt'] > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param bool $login
     * @return array
     */
    public function getTabs($login = false)
    {
        $db = $this->prepare("SELECT link, title from {$this->_config['table_tabs']} WHERE login = :login");
        $db->execute(['login' => (int)$login]);
        return $db->fetchAll();
    }
}