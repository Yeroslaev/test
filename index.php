<?php

namespace BackendDev;
use BackendDev\Model\Page\Page;
use BackendDev\Config\Connector;

class index {
    /**
     * @var Page
     */
    public $builder;

    public function __construct(
        Page $page
    )
    {
        $this->builder = $page;
    }
}
require ('config/Connector.php');
require ('Model/Page/Page.php');
$connector = new Connector();
$page = new Page($connector);
$index = new index($page);
$index->builder->sessionStart();?>
<?=$index->builder->header?>
<?=$index->builder->body?>
<?=$index->builder->footer?>

