<?php

namespace BackendDev\Model\Page;

class Page
{
    /**
     * @var \BackendDev\Config\Connector
     */
    protected $_oDb;
    /**
     * @var string
     */
    public $header;
    /**
     * @var string
     */
    protected $_title_h3;
    /**
     * @var string
     */
    protected $_html_start;
    /**
     * @var string
     */
    protected $_header_end;
    /**
     * @var string
     */
    protected $_title;
    /**
     * @var string
     */
    public $body;
    /**
     * @var string
     */
    public $footer;

    public function __construct(
        \BackendDev\Config\Connector $connector
    )
    {
        $this->_oDb = $connector;
        $this->_html_start = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        $this->_header_end = '<link rel="stylesheet" type="text/css" href="style.css"/></head>';
        $this->_title = '<title>' . $this->_oDb->getConfig('title') . '</title>';
        $this->_title_h3 = '<h3>' . $this->_oDb->getConfig('title') . '</h3>';

        $this->header = $this->html_start . $this->title . $this->header_end;
        $this->body = '<body>' . $this->title_h3 . $this->getNavMenu();
        $this->footer = "<hr /><p class='footer'>Copyright &copy; " . date('Y') . " {$this->_oDb->getConfig('title')}.</p></body></html>";
    }

    public function sessionStart()
    {
        session_start();

        if (!isset($_SESSION['user_id'])) {
            if (isset($_COOKIE['user_id']) && isset($_COOKIE['username'])) {
                $_SESSION['user_id'] = $_COOKIE['user_id'];
                $_SESSION['username'] = $_COOKIE['username'];
            }
        }
    }

    public function getNavMenu()
    {
        $navMenu = '<hr />';
        if (isset($_SESSION['username'])) {
            if ($this->_oDb->checkUsername($_SESSION['username'])) {
                $allTabs = $this->_oDb->getTabs(true);
                $navMenu = $this->generateTabs($allTabs, $navMenu);
            } else {
                unset($_SESSION['username']);
            }
        } else {
            $allTabs = $this->_oDb->getTabs();
            $navMenu = $this->generateTabs($allTabs, $navMenu);
        }
        $navMenu .= '<hr />';
        return $navMenu;
    }

    protected function generateTabs($tabs, $text)
    {
        foreach($tabs as $tab) {
            $text .= "&raquo; <a href ='{$tab['link']}' > {$tab['title']} </a> ";
        }
        return $text;
    }
}