<?php

namespace BackendDev\Model\Data;

use BackendDev\Api\Data\TicketsInterface;

class Tickets extends DataObject implements TicketsInterface
{
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED);
    }

    public function setCreatedAt($date)
    {
        return $this->setData(self::CREATED, $date);
    }

    public function getEntityId()
    {
        return $this->getData(self::ID);
    }

    public function setEntityId($entityId)
    {
        return $this->setData(self::ID, $entityId);
    }

    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED);
    }

    public function setUpdatedAt($date)
    {
        return $this->setData(self::UPDATED, $date);
    }

    public function getUserId()
    {
        return $this->getData(self::USER);
    }

    public function setUserId($userId)
    {
        return $this->setData(self::USER, $userId);
    }

}