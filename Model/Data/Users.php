<?php

namespace BackendDev\Model\Data;

use BackendDev\Api\Data\UsersInterface;

class Users extends DataObject implements UsersInterface
{
    public function getEntityId()
    {
        return $this->getData(self::ID);
    }

    public function setEntityId($entityId)
    {
        return $this->setData(self::ID, $entityId);
    }

    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    public function getLogin()
    {
        return $this->getData(self::LOGIN);
    }

    public function setLogin($login)
    {
        return $this->setData(self::LOGIN, $login);
    }

    public function getPassword()
    {
        return $this->getData(self::PASSWORD);
    }

    public function setPassword($password)
    {
        return $this->setData(self::PASSWORD, $password);
    }

    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    public function setUsername($username)
    {
        return $this->setData(self::USERNAME, $username);
    }
}