<?php

namespace BackendDev\Model\Data;

use BackendDev\Api\Data\DataObjectInterface;

abstract class DataObject implements DataObjectInterface
{
    protected $_data = [];

    public function __construct(array $data = [])
    {
        $this->_data = $data;
    }

    public function setData($key, $value = null)
    {
        if ($key === (array)$key) {
            $this->_data = $key;
        } else {
            $this->_data[$key] = $value;
        }
        return $this;
    }


    public function unsetData($key = null)
    {
        if ($key === null) {
            $this->setData([]);
        } elseif (is_string($key)) {
            if (isset($this->_data[$key]) || array_key_exists($key, $this->_data)) {
                unset($this->_data[$key]);
            }
        } elseif ($key === (array)$key) {
            foreach ($key as $element) {
                $this->unsetData($element);
            }
        }
        return $this;
    }

    public function getData($key = '')
    {
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        return null;
    }
}