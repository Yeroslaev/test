<?php

namespace BackendDev\Model\Data;

use BackendDev\Api\Data\AnswersInterface;

class Answers extends DataObject implements AnswersInterface
{
    public function getEntityId()
    {
        return $this->getData(self::ID);
    }

    public function setEntityId($entityId)
    {
        return $this->setData(self::ID, $entityId);
    }

    public function getUserId()
    {
        return $this->getData(self::USER);
    }

    public function setUserId($userId)
    {
        return $this->setData(self::USER, $userId);
    }

    public function getCreatedAt()
    {
        return $this->getData(self::CREATED);
    }

    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED, $createdAt);
    }

    public function getAnswer()
    {
        return $this->getData(self::ANSWER);
    }

    public function setAnswer($answer)
    {
        return $this->setData(self::ANSWER, $answer);
    }

    public function getTicketId()
    {
        return $this->getData(self::TICKET);
    }

    public function setTicketId($ticketId)
    {
        return $this->setData(self::TICKET, $ticketId);
    }
}