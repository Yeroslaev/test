<?php

namespace BackendDev\Model;

use BackendDev\Config\Connector;

class AbstractRepository
{
    /**
     * @var Connector
     */
    protected $_connector;

    public function __construct(
        Connector $connector
    )
    {
        $this->_connector = $connector;
    }
}